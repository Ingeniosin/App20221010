import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'App20221010';
  products: Array<any> = [];

  public form: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      name: ['', [
        Validators.required,
      ]],
      count: ['', [
        Validators.required,
      ]],
    });
  }

  onSubmit() {
    const values = this.form?.value;
    this.products.push(values);
    console.log(values);
  }

}
